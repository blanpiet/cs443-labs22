# Databases Labs at Esisar :school:

Student (public) files

Academic year 2022-23

* Laure Gonnord
 mailto `Laure.Gonnord@esisar.grenoble-inp.fr`
* Nadine Marcos

## Cours :book: et TD :pencil:

Sur [Chamilo/CS443](https://chamilo.grenoble-inp.fr/courses/ESISAR5AMCS443/index.php)


## TP :computer:

* [TP1 (1 séance) Le Tableur c'est le maaaal](TP01/README.md)

